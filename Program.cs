using System;

namespace Gym
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello! Please choose type of membership: Ordinary(o) or VIP(v)"); //выбор типа абонемента
            string memType = Console.ReadLine();
            Factory factory = getFactory(memType);
            IMembership membership = factory.getMembership();
            Console.WriteLine($"Your chose is \t{membership.Displayname}, \n\t\tit's {membership.Description}, \n\t\tprice is {membership.getPrice()}");

            Console.WriteLine("\nWanna add some options? group(g) or personal(p) trainings"); //добавление опций при желании
            string optType = Console.ReadLine();
            membership = AddOption(optType, membership);
            Console.WriteLine($"\nWell, we'he just added you some options, \n\t\tnow your price is {membership.getPrice()}");

            Console.WriteLine("\nWanna add some more options? More group(g) or personal(p) trainings"); //проверка наложения декораторов
            string optType1 = Console.ReadLine();
            membership = AddOption(optType1, membership);
            Console.WriteLine($"\nWell, we'he just added you some more options, \n\t\tnow your price is {membership.getPrice()}");

            Console.WriteLine("\n\nPlease, choose a type of payment: by card(c) or by transfer(t)"); //выбор способа оплаты
            string typeP = Console.ReadLine();
            IPayment payment = setPayType(typeP);
            payment.pay(membership);

            Console.ReadLine();
        }

        private static IMembership AddOption(string optType, IMembership memb) //метод выбора доп опции (декоратор)
        {
            switch (optType)
            {
                case "g":
                    return new GroupTrainings(memb);
                case "p":
                    return new PersonalTrainings(memb);
                default:
                    return memb;
            }
        }

        private static Factory getFactory(string memType) //создание фабрики абонемента (фабричный)
        {
            switch (memType.ToLower())
            {
                case "o":
                    return new OrdinaryFactory(100, "The ordinariest membership");
                case "v":
                    return new VIPFactory(300, "The membership, where it's all inclusive");
                default:
                    return null;
            }
        }

        private static IPayment setPayType(string typeP) //выбор способа оплаты (стратегия)
        {
            switch (typeP)
            {
                case "c":
                    return new ByCard();
                case "t":
                    return new ByTransfer();
                default:
                    return null;
            }
        }

    }
    internal interface IMembership
    {
        string Displayname { get; }
        string Description { get; set; }

        decimal getPrice();

    }

    internal abstract class Factory
    {
        public abstract IMembership getMembership();
    }

    internal class OrdinaryFactory : Factory
    {
        private readonly decimal price;
        private readonly string descr;

        public OrdinaryFactory(decimal price, string descr)
        {
            this.price = price;
            this.descr = descr;
        }

        public override IMembership getMembership()
        {
            OrdinaryMembership membership = new OrdinaryMembership(price)
            {
                Description = descr
            };
            return membership;
        }

    }

    internal class VIPFactory : Factory
    {
        private readonly decimal price;
        private readonly string descr;

        public VIPFactory(decimal price, string descr)
        {
            this.price = price;
            this.descr = descr;
        }

        public override IMembership getMembership()
        {
            VIPMembership membership = new VIPMembership(price)
            {
                Description = descr
            };
            return membership;
        }

    }

    internal class OrdinaryMembership : IMembership
    {
        private readonly string name;
        private readonly decimal price;


        public OrdinaryMembership(decimal price)
        {
            name = "Ordinary membership";
            this.price = price;
        }
        public string Description { get; set; }
        public string Displayname => name;

        public decimal getPrice()
        {
            return price;
        }
    }

    internal class VIPMembership : IMembership
    {
        private readonly string name;
        private readonly decimal price;


        public VIPMembership(decimal price)
        {
            name = "VIP membership";
            this.price = price;
        }
        public string Description { get; set; }
        public string Displayname => name;

        public decimal getPrice()
        {
            return price;
        }
    }

    internal interface IPayment // интерфейс получает конкретный абонемент, который будет оплачиваться
    {
        void pay(IMembership memb);
    }

    internal class ByCard : IPayment
    {
        void IPayment.pay(IMembership memb)             // из получаемого параметра знаем, за что платим
        {
            Console.WriteLine($"\t\tYou're chosen to pay by card. You're chose is {memb.Displayname} by {memb.getPrice()}");
        }
    }

    internal class ByTransfer : IPayment
    {
        void IPayment.pay(IMembership memb)
        {
            Console.WriteLine($"\t\tYou're chosen to pay by transfer. You're chose is {memb.Displayname} by {memb.getPrice()}");
        }
    }
     
    internal abstract class MembershipDecorator : IMembership // имеет собственную реализацию, к которой в наследниках добавляются конкретные значения
    {
        private IMembership memb;
        public MembershipDecorator(string n, IMembership membersh) : base()
        {
            Displayname = memb.Displayname + n;
            this.memb = membersh;
        }
        public string Displayname { get;  }
        public string Description { get; set; }

        public virtual decimal getPrice() => memb.getPrice();
    }

    internal class GroupTrainings : MembershipDecorator
    {
        public GroupTrainings(IMembership m) : base(" with group trainings", m) { } //к имеющемуся в базовом классе имени добавляется строка, устранили повтор кода

        public override decimal getPrice()
        {
            return base.getPrice() + 20;            //конкретный класс вызывает метод базового и добавляет своё
        }
    }

    internal class PersonalTrainings : MembershipDecorator
    {
        public PersonalTrainings(IMembership m) : base(" with personal trainings", m) { }

        public override decimal getPrice()
        {
            return base.getPrice() + 50;
        }
    }
}


